// const axios = require('axios').default;

const html = function (element, className, parent, child, innerHTML) {
    child = document.createElement(`${element}`);
    child.className = `${className}`;
    child.innerHTML = `${innerHTML}`;
    document.querySelector(`.${parent}`).appendChild(child);
};
// html(`div`, `card`, `wrapper`, `card`, ``);

// html(`div`, `card-body`, `card`, `card-body`, ``);

const valTest = function (val) {
    if (val.length > 18) {
        let newVal = `${val.substr(0, val.length - 18)}.${val.slice(
            val.length - 18
        )}`;
        console.log(newVal);
        return newVal;
    } else if (val.length == 18) {
        let newVal = `0.${val}`;
        console.log(newVal);
        return newVal;
    } else if (val.length < 18) {
        let newVal = val;
        for (let i = val.length; i < 18; i++) {
            newVal = `0${newVal}`;
        }
        newVal = `0.${newVal}`;
        console.log(newVal);
        return newVal;
    }
};

axios
    .get('https://develop.ledius.ru/api/ledius-token/dispenser-account')
    .then(response => {
        let data = response.data;
        console.log(data);
        html(`div`, `card`, `wrapper`, `card`, ``);
        html(`div`, `card_body`, `card`, `card_body`, ``);
        html(
            `div`,
            `account`,
            `card_body`,
            `account`,
            `<h2>Account</h2><P class='account-id'>ID - ${data.account.id}</P><P class='account-userId'>UserID - ${data.account.userId}</P><P class='account-address'>Address - ${data.account.address}</P>`
        );
        html(`div`, `balances`, `card_body`, `balances`, `<h2>Balances</h2>`);
        for (let i = 0; i < data.balances.length; i++) {
            html(
                `div`,
                `balances_div`,
                `balances`,
                `balances_div`,
                `<P class='balances_balance'>balance - ${valTest(
                    data.balances[i].balance
                )}</P><P class='balances_symbol'>symbol - ${
                    data.balances[i].symbol
                }</P><P class='balances_decimals'>decimals - ${
                    data.balances[i].decimals
                }</P>`
            );
        }
    });
